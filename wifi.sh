#!/bin/bash

apt-get update -y
apt-get upgrade -y
apt-get install hostapd bridge-utils dnsmasq -y

systemctl stop hostapd
systemctl stop dnsmasq

echo "denyinterfaces wlan0" >> /etc/dhcpcd.conf

echo "auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp

allow-hotplug wlan0
iface wlan0 inet static
    address 192.168.42.1
    netmask 255.255.255.0
    network 192.168.42.0
    broadcast 192.168.42.255" >> /etc/network/interfaces

echo "interface=wlan0
driver=nl80211
ssid=SmartGarden
hw_mode=g
channel=6
ieee80211n=1
wmm_enabled=1
ht_capab=[HT40][SHORT-GI-20][DSSS_CCK-40]
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_key_mgmt=WPA-PSK
wpa_passphrase=sample_password2
rsn_pairwise=CCMP" >> /etc/hostapd/hostapd.conf

sed -i 's/DAEMON_CONF=""/DAEMON_CONF="/etc/hostapd/hostapd.conf"/g' /etc/default/hostapd

echo "interface=wlan0 
listen-address=192.168.42.1
bind-interfaces 
server=8.8.8.8
domain-needed
bogus-priv
dhcp-range=192.168.42.2,192.168.42.254,24h" >> /etc/dnsmasq.conf

brctl addbr br0
brctl addif br0 wlan0

systemctl start hostapd
systemctl start dnsmasq