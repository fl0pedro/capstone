from typing import Union, List, Tuple
import _thread
from subprocess import run
import wifi
import socket

parameters = {
    "soil": ["moisture", "temperature"],
    "light": ["lux"]
}

class Parameter:
    name: str
    threshold: List[int]
    current: List[int]
    unit: str
    def __init__(self, name:str, threshold:Union[int, List[int]], unit, num = 1):
        self.name = name
        self.unit = unit
        if type(threshold) == int:
            self.threshold = [0, threshold]
        else: self.threshold = threshold
        if num == 1:
            self.current = [sum(self.threshold)/2]
        elif num == 2:
            end = sum(self.threshold)/5
            self.current = [self.threshold[0] + end, self.threshold[1] - end]

class Component:
    name: str
    parameters: List[Parameter]
    def __init__(self, name:str):
        self.name = name
    
    def get_paramaters(self):
        pass

class Pod:
    name: str
    ip: str
    components: List[Component]
    
    def __init__(self, ip, components:List[str]):
        self.ip = ip


    def set_name(self, name):
        self.name = name

class Hub:
    pods: List[Pod]
    sck: socket.socket
    addr: Tuple[str, str]

    def __init__(self):
        run("wifi.sh")
        _thread.start_new_thread(self.scan_pods)
        return

    def set_wifi(self, ssid:str, pwd:str) -> None:
        cell = wifi.Cell.all("wlan0")[0]
        scheme = wifi.Scheme.for_cell("wlan0", ssid, cell, pwd)
        scheme.save()
        scheme.activate()
        del pwd

    def scan_pods(self):
        self.sck = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.addr = ('', 9999)
        self.sck.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        try:
            self.sck.bind(self.addr)
            self.sck.listen(5)
        except socket.error as e:
            print(e)
        while True:
            client_sck, client_addr = self.sck.accept()
            components = client_sck.recv(2048)
            self.pods.append(Pod(client_addr, components.split(), client_sck))

                
    def is_connected(self) -> bool:
        return True

    def get_components(self, pod:str) -> List[Component]:
        return self.search_pods(pod).components
    
    def search_pods(self, key:str, id:str="name") -> Pod:
        if id in ["name", "mac", "ip"]:
            for pod in self.pods:
                if getattr(pod, id) == key:
                    return pod