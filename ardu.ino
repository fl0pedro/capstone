#include <WiFi.h>
#include "Adafruit_seesaw.h"
#include "Adafruit_VEML7700.h"

const char* ssid = "SmartGarden";
const char* pwd =  "sample_password2";
 
const uint16_t port = 9999;
const char * host = "192.168.42.1";

Adafruit_VEML7700 veml = Adafruit_VEML7700();
Adafruit_seesaw ss;

void sendSensor()
{  
  float tempC = ss.getTemp();
  uint16_t capread = ss.touchRead(0);
  uint16_t lux = veml.readLux();

}

void setup()
{    
  Serial.begin(115200);
  Wifi.begin(ssid, pwd)
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("...");
  }
   
  Serial.print("connected with: ");
  Serial.println(WiFi.localIP());

  ss.begin(0x36);
  veml.begin();
  veml.setGain(VEML7700_GAIN_1);
  veml.setIntegrationTime(VEML7700_IT_800MS);

  switch (veml.getGain()) {
    case VEML7700_GAIN_1: Serial.println("1"); break;
    case VEML7700_GAIN_2: Serial.println("2"); break;
    case VEML7700_GAIN_1_4: Serial.println("1/4"); break;
    case VEML7700_GAIN_1_8: Serial.println("1/8"); break;
  }
}

void loop(){
  WiFiClient client

  if (!client.connect(host, port)) return;

  client.print(sendSensor)
}