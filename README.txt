----------- hub -----------
prerequisite: raspberry pi 4B fully installed with ssh or display/keyboard access.
create virtualenv: `python3 -m venv .venv`
dependencies: `python3 -m pip install flask socket wifi`
run: `flask run`

----------- pod -----------
install libraries: `seesaw` and `VEML7700` from arduino
flash: `ardu.ino` code