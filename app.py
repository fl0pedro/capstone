from flask import Flask, redirect, render_template, request
from main import Hub

app = Flask(__name__)

# @app.route("/")
# def home():
#     return "Hello, Flask!"

# @app.route("/hello/<name>")
# def hello_there(name):
#     now = datetime.now()
#     formatted_now = now.strftime("%A, %d %B, %Y at %X")

#     # Filter the name argument to letters only using regular expressions. URL arguments
#     # can contain arbitrary text, so we restrict to safe characters only.
#     match_object = re.match("[a-zA-Z]+", name)

#     if match_object:
#         clean_name = match_object.group(0)
#     else:
#         clean_name = "Friend"

#     content = "Hello there, " + clean_name + "! It's " + formatted_now
#     return content

# @app.route("/hello/")
# @app.route("/hello/<name>")
# def hello_there(name = None):
#     return render_template(
#         "index.html",
#         name=name,
#         elements=[main.Element('pod1', 'light')],
#         date=datetime.now()
#     }

hub:Hub = Hub()
cur_dir:str = "/wifi"

@app.route("/")
def re_route():
    return redirect(cur_dir)

@app.route("/wifi", methods=["POST","GET"])
def wifi():
    if request.method == "POST":
        hub.set_wifi(request.form["ssid"], request.form["pwd"])
        workflow = "/pods"
        return redirect(workflow)
    else:
        return render_template("wifi.html")

@app.route("/pods", methods=["POST","GET"])
def pods():#pods:Union[List[main.pod], None]): 
    if hub.is_connected():
        return render_template("pods.html")

@app.route("/pods/<name>", methods=["POST","GET"])
def params(name):
    if hub.is_connected() and request.method == "POST":
            # main.update_params() # make this modular
            pass
    else:
        return render_template("pod.html", components=hub.pods)

def data(name):
    if hub.is_connected():
        return

@app.errorhandler(404)
def not_found(error):
    return render_template("404.html")